# Doug's SendHub API Messenging System
To build, please run

    pod install

### Issues

I'm having some issues figuring out the best way to "Refresh" the contacts.
Keeping the local store in sync with the server is turning out to be a non-trivial problem because the RestKit pagination I chose to use has a queuing issue.
[https://github.com/RestKit/RestKit/issues/1578]