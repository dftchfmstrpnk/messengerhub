//
//  AppDelegate.m
//  MessengerHub
//
//  Created by Douglas Blaalid on 10/25/14.
//  Copyright (c) 2014 Douglas Blaalid. All rights reserved.
//
//  DMH class prefix == Doug's Messenger Hub.

#import "AppDelegate.h"
#import "DMHContactsViewController.h"

#import <RestKit/RestKit.h>
//  I chose to use RestKit for a variety of reasons:
//      1. I almost always use AFNetworking for my RESTful applications. The interface is clean,
//      it allows for block callbacks in success and failure, it is automatically performed off the
//      main thread, ... It allows me to not reinvent the wheel
//      2. CoreData is never fun to reinvent. Keeping your local DB in sync with the web is not a trival
//      task. I like the way that RestKit has dealt with this problem and it abstracts away the Cocoa
//      intricacies
//      3. It provides a system for binding your application's data model with the JSON documents from the web.
//      Again, a non-trivial task that can be implemeneted with KVC.




@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Set RestKit logs because there's a lot behind the scenes. Don't ship with this.
    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelDebug);

    
    NSURLCache *URLCache = [[NSURLCache alloc] initWithMemoryCapacity:8 * 1024 * 1024 diskCapacity:20 * 1024 * 1024 diskPath:nil];
    [NSURLCache setSharedURLCache:URLCache];
    
    // Let AFNetowrking handle activity indicator
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    
    // Landing controller
    DMHContactsViewController *contactsController = [DMHContactsViewController new];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:contactsController];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = navigationController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
