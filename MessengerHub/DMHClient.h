//
//  DMHClient.h
//  MessengerHub
//
//  Created by Douglas Blaalid on 10/26/14.
//  Copyright (c) 2014 Douglas Blaalid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@class DMHContact;
@class DMHMessage;

@interface DMHClient : NSObject

@property (nonatomic, strong) RKObjectManager *objectManager;
@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong) RKManagedObjectStore *managedObjectStore;
@property (nonatomic, strong) RKPaginator *contactPaginator;

+ (instancetype)sharedClient;
+ (void)presentError:(NSError *)error;
+ (void)presentInfo:(NSString *)info;
- (void)postMessage:(DMHMessage *)message withCompleteCallback:(void (^)(BOOL))complete;
- (void)resetLocalStore;

@end
