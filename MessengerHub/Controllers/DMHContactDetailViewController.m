//
//  DMHContactDetailViewController.m
//  MessengerHub
//
//  Created by Douglas Blaalid on 10/26/14.
//  Copyright (c) 2014 Douglas Blaalid. All rights reserved.
//

#import "DMHContactDetailViewController.h"
#import "DMHContact.h"
#import "DMHComposeViewController.h"

@interface DMHContactDetailViewController ()



@end

@implementation DMHContactDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.clearsSelectionOnViewWillAppear = YES;
    
    // Title up!
    [self setTitle:self.contact.name];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 2;
            break;
            
        default:
            return 1;
            break;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    switch (indexPath.section) {
        case 0:
            cell = [tableView dequeueReusableCellWithIdentifier:@"InfoCell"];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:@"InfoCell"];
            }
            switch (indexPath.row) {
                case 0:
                    [cell.textLabel setText:@"Name:"];
                    [cell.detailTextLabel setText:self.contact.name];
                    break;
                    
                case 1:
                    [cell.textLabel setText:@"Number:"];
                    [cell.detailTextLabel setText:self.contact.number];
                default:
                    break;
            }
            break;
            
        default:
            cell = [tableView dequeueReusableCellWithIdentifier:@"MessageCell"];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MessageCell"];
            }
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell.textLabel setText:@"Send Message"];
            break;
    }
    
    return cell;
}


#pragma mark - Table view delegate

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            return NO;
            
        default:
            return YES;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 1:
        {
            DMHComposeViewController *composeView = [[DMHComposeViewController alloc] initWithStyle:UITableViewStyleGrouped];
            [composeView setContact:self.contact];
            UINavigationController *composeNav = [[UINavigationController alloc] initWithRootViewController:composeView];
            [self presentViewController:composeNav animated:YES completion:nil];
            break;
        }
        default:
            // Do nothing.
            break;
    }
}


@end
