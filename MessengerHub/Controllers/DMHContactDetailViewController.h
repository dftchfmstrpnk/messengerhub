//
//  DMHContactDetailViewController.h
//  MessengerHub
//
//  Created by Douglas Blaalid on 10/26/14.
//  Copyright (c) 2014 Douglas Blaalid. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMHContact;

@interface DMHContactDetailViewController : UITableViewController

@property (nonatomic, strong) DMHContact *contact;

@end
