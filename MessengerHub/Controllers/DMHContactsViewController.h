//
//  DMHContactsViewController.h
//  MessengerHub
//
//  Created by Douglas Blaalid on 10/25/14.
//  Copyright (c) 2014 Douglas Blaalid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMHContactsViewController : UITableViewController

@end
