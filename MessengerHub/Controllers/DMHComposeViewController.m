//
//  DMHComposeViewController.m
//  MessengerHub
//
//  Created by Douglas Blaalid on 10/26/14.
//  Copyright (c) 2014 Douglas Blaalid. All rights reserved.
//

#import "DMHComposeViewController.h"
#import "DMHMessageCell.h"
#import "DMHClient.h"
#import "DMHMessage.h"
#import "DMHContact.h"
#import <RestKit/RestKit.h>
#import <MBProgressHUD/MBProgressHUD.h>


@interface DMHComposeViewController ()
@property (nonatomic, strong) DMHClient *client;
@property (nonatomic, strong) UITextView *messageView;
@end

@implementation DMHComposeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:@"Compose"];
    
    UIBarButtonItem *sendButton = [[UIBarButtonItem alloc] initWithTitle:@"Send" style:UIBarButtonItemStyleDone target:self action:@selector(sendMessage)];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismiss)];
    [self.navigationItem setRightBarButtonItem:sendButton];
    [self.navigationItem setLeftBarButtonItem:cancelButton];
}

- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendMessage
{
    if (self.messageView) {
        // Check for text
        if (self.messageView.text.length > 0) {
            // Send
            [self.messageView resignFirstResponder];    // Clear the screen.
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            DMHMessage *message = [DMHMessage new]; // TODO: Class factory method
            message.text = self.messageView.text;
            message.contacts = @[self.contact.identifier];
            [self.client postMessage:message withCompleteCallback:^(BOOL success){
                [hud hide:YES];
                // Only dismess compose if we succeed
                if (success) {
                    [self dismiss];
                }
                else {
                    [self.messageView becomeFirstResponder]; // Need this back. There was an error.
                }
            }];
        }
        else {
            // No text!
            [DMHClient presentInfo:@"Please compose a message."];
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DMHMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageCell"];
    if (!cell) {
        cell = (DMHMessageCell *)[[[NSBundle mainBundle] loadNibNamed:@"DMHMessageCell" owner:nil options:nil] objectAtIndex:0];
    }
    [cell.messageView setDelegate:self];
    [cell.messageView becomeFirstResponder];
    self.messageView = cell.messageView;
    return cell;
}

#pragma mark - Table view delegate

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 140.0f;
}

#pragma mark - Lazy accessors

- (DMHClient *)client
{
    if (!_client) {
        _client = [DMHClient sharedClient];
    }
    return _client;
}


@end
