//
//  DMHContactsViewController.m
//  MessengerHub
//
//  Created by Douglas Blaalid on 10/25/14.
//  Copyright (c) 2014 Douglas Blaalid. All rights reserved.
//

#import "DMHContactsViewController.h"
#import "DMHContact.h"
#import "DMHClient.h"
#import "DMHConstants.h"
#import "DMHContactDetailViewController.h"
#import <SVPullToRefresh/SVPullToRefresh.h>

@interface DMHContactsViewController () <NSFetchedResultsControllerDelegate>
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) DMHClient *client;
@property (nonatomic, strong) RKPaginator *paginator;
@property (nonatomic, strong) dispatch_queue_t serialPageRequestQueue;
- (NSUInteger)latestLocalPage;
@end

@implementation DMHContactsViewController {
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.clearsSelectionOnViewWillAppear = YES;
    
    // Title up!
    [self setTitle:@"SendHub Contacts"];
    
    // Refresh control
//    self.refreshControl = [[UIRefreshControl alloc] init];
//    [self.refreshControl addTarget:self action:@selector(purge) forControlEvents:UIControlEventValueChanged];
    
    // Fetched results are a great way to only keep what we need in memory and leave the rest in the data store
    [self createFetchedResultsController];
 
    // Set up paginator
    self.paginator = self.client.contactPaginator;
    [self.paginator setCompletionBlockWithSuccess:^(RKPaginator *paginator, NSArray *objects, NSUInteger page) {
        // The entity mapping should land this in the object store
    } failure:^(RKPaginator *paginator, NSError *error) {
        [DMHClient presentError:error];
    }];
    
    //Serial queue for the page loads
    self.serialPageRequestQueue = dispatch_queue_create("com.daft.MessengerHub.pages", DISPATCH_QUEUE_SERIAL);
    dispatch_async(self.serialPageRequestQueue, ^{
        [self.paginator loadPage:[self latestLocalPage] + 1];
    });
}

// This is finicky... Need to remove from the store but it crashes because the table view reloads and tries to draw with nothing
//- (void)purge
//{
//    [self.client resetLocalStore];
//    [self createFetchedResultsController];
//    [self.refreshControl endRefreshing];
//    [self.paginator loadPage:1];
//}

- (void)createFetchedResultsController
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"DMHContact"];
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    fetchRequest.sortDescriptors = @[descriptor];
    self.fetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.client.managedObjectStore.mainQueueManagedObjectContext
                                          sectionNameKeyPath:nil
                                                   cacheName:nil];
    [self.fetchedResultsController setDelegate:self];
    NSError *error = nil;
    BOOL fetchSuccess = [self.fetchedResultsController performFetch:&error];
    if (!fetchSuccess) {
        [DMHClient presentError:error];
    }
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController.sections objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ContactCell"];
    }
    
    // Simply set the name of the contact to the cell title
    DMHContact *contact = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell.textLabel setText:contact.name];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
        id<NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController.sections objectAtIndex:indexPath.section];
        NSUInteger count = [sectionInfo numberOfObjects];
        if(indexPath.row == count - 1){
            dispatch_async(self.serialPageRequestQueue, ^{
                [self.paginator loadPage:[self latestLocalPage] + 1];
            });
        }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DMHContact *contact = [self.fetchedResultsController objectAtIndexPath:indexPath];
    DMHContactDetailViewController *detailView = [[DMHContactDetailViewController alloc] initWithStyle:UITableViewStyleGrouped];
    [detailView setContact:contact];
    [self.navigationController pushViewController:detailView animated:YES];
}

#pragma mark - Lazy accessors

- (DMHClient *)client
{
    if (!_client) {
        _client = [DMHClient sharedClient];
    }
    return _client;
}

- (NSUInteger)latestLocalPage
{
    id<NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController.sections objectAtIndex:0];
    NSUInteger count = [sectionInfo numberOfObjects];
    return count/20; // Intentional integer truncation
}

@end
