//
//  DMHMessageCell.h
//  MessengerHub
//
//  Created by Douglas Blaalid on 10/26/14.
//  Copyright (c) 2014 Douglas Blaalid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMHMessageCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UITextView *messageView;

@end
