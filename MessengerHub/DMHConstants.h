//
//  DMHConstants.h
//  MessengerHub
//
//  Created by Douglas Blaalid on 10/25/14.
//  Copyright (c) 2014 Douglas Blaalid. All rights reserved.
//

#ifndef MessengerHub_DMHConstants_h
#define MessengerHub_DMHConstants_h

extern NSString *const kAPIBaseURL;
extern NSString *const kAPIKey;
extern NSString *const kAPIUsername;
extern NSString *const kAPIContactsPath;
extern NSString *const kAPIContactsPaginatorPath;
extern NSString *const kAPIMessagePath;
extern NSString *const kAPIMessagePathWithCreds;

#endif
