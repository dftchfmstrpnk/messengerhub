//
//  DMHConstants.m
//  MessengerHub
//
//  Created by Douglas Blaalid on 10/25/14.
//  Copyright (c) 2014 Douglas Blaalid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DMHConstants.h"

NSString *const kAPIBaseURL = @"https://api.sendhub.com";
NSString *const kAPIKey = @"21b888ebbeb1acdc36340ed7be4f477eec7f9e54";
NSString *const kAPIUsername = @"9499398474";
NSString *const kAPIContactsPath = @"/v1/contacts/";
NSString *const kAPIContactsPaginatorPath = @"/v1/contacts/?api_key=%@&username=%@&limit=:perPage&offset=:offset";
NSString *const kAPIMessagePath = @"/v1/messages/";
NSString *const kAPIMessagePathWithCreds = @"/v1/messages/?api_key=%@&username=%@";