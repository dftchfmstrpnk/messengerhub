//
//  DMHClient.m
//  MessengerHub
//
//  Created by Douglas Blaalid on 10/26/14.
//  Copyright (c) 2014 Douglas Blaalid. All rights reserved.
//

#import "DMHClient.h"
#import "DMHConstants.h"
#import "DMHMessage.h"


@interface DMHClient()

// Mappers (KVC object <-> JSON)
@property (nonatomic, strong) RKEntityMapping *contactEntityMapping;
@property (nonatomic, strong) RKObjectMapping *paginationMapping;
@property (nonatomic, strong) RKObjectMapping *messageMapping;
@property (nonatomic, strong) RKObjectMapping *messageResponseMapping;

// Descriptors (How to describe incoming JSON)
@property (nonatomic, strong) RKResponseDescriptor *contactResponseDescriptor;
@property (nonatomic, strong) RKRequestDescriptor *messageRequestDescriptor;

@end


@implementation DMHClient

#pragma mark - Class methods

+ (instancetype)sharedClient
{
    static DMHClient *client = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        client = [[self alloc] init];
    });
    return client;
}

// Let the client class know how to present any error
+ (void)presentError:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"Darn" otherButtonTitles:nil];
    [alert show];
}

+ (void)presentInfo:(NSString *)info
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:info
                                                   delegate:nil
                                          cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

#pragma mark - inits

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initManager];
        [self initMappingsAndDescriptors];
    }
    return self;
}

// This is called from init. Do not use accessors because self maybe be still in limbo.
//
// Set up the object manager. This was original factored as DMHContactManager but I would prefer
// a more general manager. One that manages mapping all requests and responses from the SendHub api.
- (void)initManager
{
    _objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:kAPIBaseURL]];
    
    // SendHub uses JSON only
    _objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
    [_objectManager.HTTPClient setDefaultHeader:@"Content-Type" value:@"application/json"];
    
    // Security is important
    [_objectManager.HTTPClient setAllowsInvalidSSLCertificate:NO];
    
    [RKObjectManager setSharedManager:_objectManager];
    
    // Now set up object store for CoreData
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    _managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:_managedObjectModel];
    _objectManager.managedObjectStore = _managedObjectStore;
    
    // Create store coordinator for Contacts objects
    [_objectManager.managedObjectStore createPersistentStoreCoordinator];
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"DMHContacts.sqlite"];
    NSError *error;
    NSPersistentStore *persistentStore =
    [_objectManager.managedObjectStore addSQLitePersistentStoreAtPath:storePath
                                               fromSeedDatabaseAtPath:nil
                                                    withConfiguration:nil
                                                              options:nil
                                                                error:&error];
    // Not sure how to handle this in production...
    NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    // Create the object context and cache (to prevent dups)
    [_objectManager.managedObjectStore createManagedObjectContexts];
    _objectManager.managedObjectStore.managedObjectCache =
        [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:_managedObjectStore.persistentStoreManagedObjectContext];
    [RKManagedObjectStore setDefaultStore:_objectManager.managedObjectStore];
}


// This is called from init. Do not use accessors because self maybe be still in limbo.
//
// Mappings have the configuration for transforming object representations as expressed by key-value coding keypaths.
// Descriptors define an object mappable response or request that may be encournted from a remote web application in
// terms of an object mapping, a key path, a SOCKit pattern for matching the URL, and a set of status codes that define
// the circumstances in which the mapping is appropriate for a given response.
- (void)initMappingsAndDescriptors
{
    NSIndexSet *successStatus = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
    NSIndexSet *failureStatus = RKStatusCodeIndexSetForClass(RKStatusCodeClassClientError);
    
    // This will map a DMHContact entity to the params of the incoming response.
    _contactEntityMapping = [RKEntityMapping mappingForEntityForName:@"DMHContact" inManagedObjectStore:_managedObjectStore];
    [_contactEntityMapping addAttributeMappingsFromDictionary: @{ @"name"            : @"name",
                                                                  @"number"          : @"number",
                                                                  @"id"              : @"identifier",
                                                                  @"date_created"    : @"dateCreated",
                                                                  @"date_modified"   : @"dateModified" }];
    // Unique by id (hopefully)
    [_contactEntityMapping setIdentificationAttributes: @[ @"identifier" ]];
    
    // Need to describe how to handle the contacts path response. We want the "objects" key data.
    _contactResponseDescriptor =
        [RKResponseDescriptor responseDescriptorWithMapping:_contactEntityMapping
                                                     method:RKRequestMethodGET
                                                pathPattern:kAPIContactsPath
                                                    keyPath:@"objects"
                                                statusCodes:successStatus];
    [_objectManager addResponseDescriptor:_contactResponseDescriptor];
    
    
    // Map the Message class outbound. This might not be necessary for such a simple symetric mapping.
    // Down the road we could add much more to the message class.
    _messageMapping = [RKObjectMapping mappingForClass:[DMHMessage class]];
    [_messageMapping addAttributeMappingsFromDictionary: @{ @"contacts"    : @"contacts",
                                                            @"text"        : @"text" }];
    
    // We need to know how to form the request. It uses the inverse mapping (object -> web representation)
    // but that is mainly a moot point because it is symetric right now.
    _messageRequestDescriptor =
        [RKRequestDescriptor requestDescriptorWithMapping:[_messageMapping inverseMapping]
                                              objectClass:[DMHMessage class]
                                              rootKeyPath:nil
                                                   method:RKRequestMethodPOST];
    [_objectManager addRequestDescriptor:_messageRequestDescriptor];
    
    // Need to know how to get the pagination info from the GET responses.
    // We use the Key Value Path to specify where in the incoming data object to go for the pagination data memebers.
    // These (perPage, offset, objectCount, etc) are predetermined by the RKPaginator class
    _paginationMapping = [RKObjectMapping mappingForClass:[RKPaginator class]];
    [_paginationMapping addAttributeMappingsFromDictionary: @{ @"meta.limit"       : @"perPage",
                                                               @"meta.offset"      : @"offset",
                                                               @"meta.total_count" : @"objectCount" }];
    [_objectManager setPaginationMapping:_paginationMapping];
    
    NSString *paginationPath = [NSString stringWithFormat:kAPIContactsPaginatorPath, kAPIKey, kAPIUsername];
    _contactPaginator = [_objectManager paginatorWithPathPattern:paginationPath];
    [_contactPaginator setManagedObjectContext:_objectManager.managedObjectStore.mainQueueManagedObjectContext];
    // 20 seems to be a good number for regular use. Ideally, I would want to fetch it all at once, so we could search to object store later without have to make any more web requests.
    [_contactPaginator setPerPage:20];
}




// Post a message with this client.
- (void)postMessage:(DMHMessage *)message withCompleteCallback:(void (^)(BOOL))complete
{
    NSString *messagingPathWithCreds = [NSString stringWithFormat:kAPIMessagePathWithCreds, kAPIKey, kAPIUsername];
    [self.objectManager postObject:message
                              path:messagingPathWithCreds
                        parameters:nil
                           success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                               RKLogInfo(@"Message post mapping complete");
                           }
                           failure:^(RKObjectRequestOperation *operation, NSError *error) {
                               // There is no descriptor to map the /v1/message response to so it will fail even though the HTTP operation may succeed... This is intentional so I didn't need to right a descriptor for the messages response
                               
                               AFHTTPRequestOperation *op = operation.HTTPRequestOperation;
                               NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:op.responseData options:NSJSONReadingMutableLeaves error:nil];

                               if ([op hasAcceptableStatusCode]) {
                                   // We succeeded
                                   RKLogInfo(@"Post success, message sent");
                                   complete(YES);
                                   if ([dict objectForKey:@"acknowledgment"]) {
                                       [DMHClient presentInfo:[dict objectForKey:@"acknowledgment"]];
                                   }
                               }
                               else {
                                   RKLogError(@"Post failed: %@", error.userInfo);
                                   complete(NO);
                                   if ([dict objectForKey:@"message"]) {
                                       [DMHClient presentInfo:[dict objectForKey:@"message"]];
                                   }
                                   else {
                                       // We didn't get something we could parse out. This may be a restkit level error though.
                                       [DMHClient presentError:error];
                                   }
                               }
                           }];
}

// The idea here is that if we want to get updates from the server, it would probably be easiest to destroy our local store and just reload. However, this isn't good user experience... I just haven't worked with NSPersistentStoreCoordinators enough to know the best way to do this.
- (void)resetLocalStore
{
    [self.managedObjectStore resetPersistentStores:nil];
//    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
//        NSManagedObjectContext *managedObjectContext = [RKManagedObjectStore defaultStore].persistentStoreManagedObjectContext;
//        [managedObjectContext performBlockAndWait:^{
//            NSError *error = nil;
//            for (NSEntityDescription *entity in [RKManagedObjectStore defaultStore].managedObjectModel) {
//                NSFetchRequest *fetchRequest = [NSFetchRequest new];
//                [fetchRequest setEntity:entity];
//                [fetchRequest setIncludesSubentities:NO];
//                NSArray *objects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
//                if (! objects) RKLogWarning(@"Failed execution of fetch request %@: %@", fetchRequest, error);
//                for (NSManagedObject *managedObject in objects) {
//                    [managedObjectContext deleteObject:managedObject];
//                }
//            }
//            BOOL success = [managedObjectContext save:&error];
//            if (!success) RKLogWarning(@"Failed saving managed object context: %@", error);
//        }];
//    }];
//    [operation setCompletionBlock:^{
//        // Do stuff once the truncation is complete
//    }];
//    [operation start];
}

@end
