//
//  DMHMessage.h
//  MessengerHub
//
//  Created by Douglas Blaalid on 10/26/14.
//  Copyright (c) 2014 Douglas Blaalid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMHMessage : NSObject

@property (nonatomic, strong) NSArray *contacts;
@property (nonatomic, strong) NSString *text;

@end
