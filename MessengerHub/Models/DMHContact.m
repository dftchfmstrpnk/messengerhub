//
//  DMHContact.m
//  MessengerHub
//
//  Created by Douglas Blaalid on 10/25/14.
//  Copyright (c) 2014 Douglas Blaalid. All rights reserved.
//

#import "DMHContact.h"

@implementation DMHContact

@dynamic name;
@dynamic number;
@dynamic identifier;
@dynamic dateCreated;
@dynamic dateModified;

@end
